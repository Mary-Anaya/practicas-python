def total_factura(cant_sin_iva, iva):
    """"
    Ejercicio #1
    Descripcion: Funcion que calcula el total de una factura despues de aplicarle IVA
    Parametros: can_sin_iva, iva
    return: total de la factura + iva
    """

    if iva == 0:
        total = cant_sin_iva + (cant_sin_iva * 0.21)
    else:
        total = cant_sin_iva + (cant_sin_iva * iva / 100)
    if iva < 0:
        return print('El valor ingresado no puede ser menor a 0')
    return total

if __name__ == "__main__":
    try:
        cant_sin_iva = abs(int(input('Ingrese la cantidad sin IVA: ')))
        iva = float(input('Ingrese el porcentaje de IVA a aplicar: '))
        print("El total de la factura es: ", total_factura(cant_sin_iva, iva))

    except Exception as error:
       print(error)

