
def ex_2_4(n):
    """"
    Ejercicio #2
    Descripcion: Dado una cantidad de segundos esta funcion calcula, el tiempo en dias, horas, minutos y segundos
    Parametros: n = segundos a convertir
    """
    minutos = n // 60
    segundos_rest = n % 60
    horas = minutos // 60
    minutos_rest = minutos % 60
    dias = horas //24

    tiempo = {'Dias': dias, 'Horas': horas, 'Minutos': minutos_rest, 'Segundos': segundos_rest}

    return tiempo

if __name__ == "__main__":

    try:
        segundos = abs(int(input('Ingrese la cantidad de segundos: ')))
        print(ex_2_4(segundos))
    except ValueError:
        print('El tipo de dato ingresado no puede ser procesado')























