diccionario_monedas = {
    'dolar': {
        'pesos': 3986,
        'euro': 0.92,
        'yen': 110.5,
        'libras': 0.77,
    },
    'pesos': {
        'dolar': 0.0028,
        'euro': 0.0008,
        'yen': 0.0012,
        'libras': 0.0007,
    },
    'euro': {
        'dolar': 1.13,
        'pesos': 6.78,
        'yen': 0.0089,
        'libras': 0.71,
    },
    'yen': {
        'dolar': 0.0091,
        'pesos': 0.0012,
        'euro': 0.0089,
        'libras': 0.0071,
    },
    'libras': {
        'dolar': 1.31,
        'pesos': 0.78,
        'euro': 1.13,
        'yen': 0.0071,
    },
}


def conversor(moneda_base, moneda_destino, cantidad):

    """"
    Ejercicio #3
    Descripcion: Funcion que realiza conversion de moneda, de un diccionario previamente establecido, con 5 tipos de moneda
    Parametros: moneda_base, moneda_destino y cantidad a convertir
    return: conversion y mensaje en formato
    """

    siglas_monedas = {
        'dolar': 'USD',
        'pesos': 'COP',
        'euro': 'EUR',
        'yen': 'JPY',
        'libras': 'GBP',
    }

    sigla_moneda_base = siglas_monedas[moneda_base]
    sigla_moneda_destino = siglas_monedas[moneda_destino]

    if moneda_base == moneda_destino:
        return mostrar_mensaje(cantidad, cantidad, sigla_moneda_base, sigla_moneda_destino)

    total = cantidad * diccionario_monedas[moneda_base][moneda_destino]
    return mostrar_mensaje(cantidad, total, sigla_moneda_base, sigla_moneda_destino)


def mostrar_mensaje(cantidad, total, sigla_moneda_base, sigla_moneda_destino):
    return f'La conversion fue la siguiente: {cantidad} {sigla_moneda_base} = {total} {sigla_moneda_destino}'


if __name__ == "__main__":

    moneda_base = input('Ingrese moneda base: dolar, pesos, euro, yen o libras ')

    while moneda_base != 'dolar' and moneda_base != 'pesos' and moneda_base != 'yen' and moneda_base != 'euro' and moneda_base != 'libras':
        print('La moneda ingresada no esta en la base de datos')
        moneda_base = input('Ingrese moneda base: dolar, pesos, euro, yen o libras ')

    moneda_destino = input('Ingrese moneda destino: dolar, pesos, euro, yen o libras ')

    while moneda_destino != 'dolar' and moneda_destino != 'pesos' and moneda_destino != 'yen' and moneda_destino != 'euro' and moneda_destino != 'libras':
        print('La moneda ingresada no esta en la base de datos')
        moneda_destino = input('Ingrese moneda destino: dolar, pesos, euro, yen o libras ')

    cantidad = float(input(f'Ingrese la cantidad de {moneda_base} a convertir: '))


    print(conversor(moneda_base , moneda_destino, cantidad))

